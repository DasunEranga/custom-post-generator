<?php

/**
 * @package deabPlugin 
 */

namespace Inc\Base;

class Enqueue
{

    public function register()
    {
        add_action('admin_enqueue_scripts', array($this, 'enqueue'));
    }

    function enqueue()
    {
        wp_enqueue_style('deab_plugin_style', PLUGIN_URL . 'assets/css/style.css', array(), '1.0.0', 'all');
        wp_enqueue_script('deab_plugin_script', PLUGIN_URL . 'assets/js/custom.js', array(), '1.0.0', false);
    }
}
