<?php

/**
 * @package deabPlugin 
 */

namespace Inc;

final class Init
{
    /**
     *  Store all the clsses inside an array
     *  @return array full list of classes
     */
    public static function get_services()
    {
        return [
            Pages\Admin::class,
            Pages\CustomPost::class,
            Pages\CustomMeta::class,
            Base\Enqueue::class
        ];
    }

    /**
     * Loop through the classes, Initialize them, and call the register() method if it exist  
     */
    public static function register_services()
    {
        foreach (self::get_services() as $class) {
            $service = self::lounch($class);
            if (method_exists($service, 'register')) {

                $service->register();
            }
        }
    }

    /**
     * Inistialize the class
     * @param $class : class from the service array
     * @return class instance   : new instance of the class
     */
    private static function lounch($class)
    {
        $service = new $class;
        return $service;
    }
}
