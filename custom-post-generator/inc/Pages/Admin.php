<?php

/**
 * @package deabPlugin 
 */

namespace Inc\Pages;

class Admin
{

    public function register()
    {
        // add_action('admin_menu', array($this, 'add_admin_page'));
        add_action('admin_init', array($this, 'deab_custom_post_settings'));
        add_filter("plugin_action_links_" . PLUGIN_BASE_NAME, array($this, 'settings_link'));
    }

    public function add_admin_page()
    {
        add_menu_page('Deab Plugin', 'Deab', 'manage_options', 'deab_plugin', array($this, 'admin_index'), 'dashicons-buddicons-groups', 110);
    }

    public function admin_index()
    {
        require_once PLUGIN_PATH . 'template/index.php';
    }

    // adding custom links
    public function settings_link($link)
    {
        $_setting_link = "<a href='admin.php?page=deab_plugin'>Settings</a>";
        array_push($link, $_setting_link);
        return $link;
    }

    public function deab_custom_post_settings()
    {
        // ----- Name -----
        register_setting('deab-setting-group', 'deab_post_type');
        register_setting('deab-setting-group', 'deab_post_label');

        add_settings_section('deab-sidebar-options', 'Sidebar Options', array($this, 'deab_sidebar_options'), 'deab_plugin');

        add_settings_field('post-type', 'Post Type', array($this, 'post_type_name'), 'deab_plugin', 'deab-sidebar-options');
        add_settings_field('post-label', 'Post Label', array($this, 'post_label_name'), 'deab_plugin', 'deab-sidebar-options');
    }

    public function deab_sidebar_options()
    {
        echo "deAB settings goes here";
        echo "<input type='button' id='cpt_gen_btn' class='button button-primary' value='Add new'>";
    }

    public function post_type_name()
    {
        $_post_type = esc_attr(get_option('deab_post_type'));

        echo '<input type="text" name="deab_post_type" value="' . $_post_type . '" placeholder="Post Type"> <p class="description">Changing this post type name after first time, will cause to lose all post data. (Must not exceed 20 characters and may only contain lowercase alphanumeric characters, dashes, and underscores.)</p>';
    }

    public function post_label_name()
    {
        $_post_label = esc_attr(get_option('deab_post_label'));

        echo '<input type="text" name="deab_post_label" value="' . $_post_label . '" placeholder="Post Label"><p class="description">Name of the post type shown in the menu.</p>';
    }
}
