<?php

/**
 * @package deabPlugin 
 */

namespace Inc\Pages;

class CustomMeta
{

    public function register()
    {
        add_action('add_meta_boxes', array($this, 'deab_cpt_meta'));
        add_action('save_post', array($this, 'save_deab_cpt_meta_fields'));
    }

    public function deab_cpt_meta()
    {
        add_meta_box(
            'deab_cpt_meta_box',
            'Custom Post Type register settings',
            array($this, 'deab_cpt_meta_fields'),
            'deab_cpt_generater', // $screen
            'normal', // $context
            'high' // $priority
        );
    }

    public function deab_cpt_meta_fields()
    {
        global $post;
        $meta = get_post_meta($post->ID, 'deab_register_post_settings', true); ?>

        <input type="hidden" name="your_meta_box_nonce" value="<?php echo wp_create_nonce(basename(__FILE__)); ?>">

        <!-- Post type key -->
        <section class="post-setting post-setting--post-basic">
            <h2><strong>Post Settings</strong></h2>

            <div class="field-wrapper">
                <input required type="text" name="deab_register_post_settings[deab_post]" id="deab_register_post_settings[deab_post]" class="regular-text" placeholder="Post type key" value="<?php echo (is_array($meta) && isset($meta['deab_post'])) ? $meta['deab_post'] : ''; ?>">
                <p class="description"> Post type key. Must not exceed 20 characters and may only contain lowercase alphanumeric characters, dashes, and underscores.</p>
            </div>

        </section>

        <!-- Menu position -->
        <section class="post-setting post-setting--rest">
            <h2><strong>Position in the menu</strong></h2>

            <select name="deab_register_post_settings[deab_menu_position]" id="deab_register_post_settings[deab_menu_position]">
                <option value="25" <?php if (is_array($meta) && isset($meta['deab_menu_position']) && $meta['deab_menu_position'] == '25') {
                                        echo 'selected="selected"';
                                    } ?>>below comments</option>
                <option value="5" <?php if (is_array($meta) && isset($meta['deab_menu_position']) && $meta['deab_menu_position'] == '5') {
                                        echo 'selected="selected"';
                                    } ?>>below Posts</option>
                <option value="10" <?php if (is_array($meta) && isset($meta['deab_menu_position']) && $meta['deab_menu_position'] == '10') {
                                        echo 'selected="selected"';
                                    } ?>>below Media</option>
                <option value="15" <?php if (is_array($meta) && isset($meta['deab_menu_position']) && $meta['deab_menu_position'] == '15') {
                                        echo 'selected="selected"';
                                    } ?>>below Links</option>
                <option value="20" <?php if (is_array($meta) && isset($meta['deab_menu_position']) && $meta['deab_menu_position'] == '20') {
                                        echo 'selected="selected"';
                                    } ?>>below Pages</option>
                <option value="60" <?php if (is_array($meta) && isset($meta['deab_menu_position']) && $meta['deab_menu_position'] == '60') {
                                        echo 'selected="selected"';
                                    } ?>>below first separator</option>
                <option value="65" <?php if (is_array($meta) && isset($meta['deab_menu_position']) && $meta['deab_menu_position'] == '65') {
                                        echo 'selected="selected"';
                                    } ?>>below Plugins</option>
                <option value="70" <?php if (is_array($meta) && isset($meta['deab_menu_position']) && $meta['deab_menu_position'] == '70') {
                                        echo 'selected="selected"';
                                    } ?>>below Users</option>
                <option value="75" <?php if (is_array($meta) && isset($meta['deab_menu_position']) && $meta['deab_menu_position'] == '75') {
                                        echo 'selected="selected"';
                                    } ?>>below Tools</option>
                <option value="80" <?php if (is_array($meta) && isset($meta['deab_menu_position']) && $meta['deab_menu_position'] == '80') {
                                        echo 'selected="selected"';
                                    } ?>>below Settings</option>
                <option value="100" <?php if (is_array($meta) && isset($meta['deab_menu_position']) && $meta['deab_menu_position'] == '100') {
                                        echo 'selected="selected"';
                                    } ?>>below second separator</option>
            </select>
            <p class="description">The position in the menu order the post type should appear.(defaults to below Comments)</p>
        </section>

        <!-- Post Support -->
        <section class="post-setting post-setting--post-support">
            <h2><strong> Post Supports</strong></h2>

            <!-- Editor -->
            <div class="field-wrapper">
                <label for="deab_register_post_settings[editor]">Editor</label>
                <input type="checkbox" name="deab_register_post_settings[editor]" value="editor" <?php echo (is_array($meta) && isset($meta['editor'])) ? 'checked' : ''; ?>>
            </div>

            <!-- Thumbnail -->
            <div class="field-wrapper">
                <label for="deab_register_post_settings[thumbnail]">Thumbnail</label>
                <input type="checkbox" name="deab_register_post_settings[thumbnail]" value="thumbnail" <?php echo (is_array($meta) && isset($meta['thumbnail'])) ? 'checked' : ''; ?>>
            </div>

            <!-- Excerpt -->
            <div class="field-wrapper">
                <label for="deab_register_post_settings[excerpt]">Excerpt</label>
                <input type="checkbox" name="deab_register_post_settings[excerpt]" value="excerpt" <?php echo (is_array($meta) && isset($meta['excerpt'])) ? 'checked' : ''; ?>>
            </div>

            <!-- Author -->
            <div class="field-wrapper">
                <label for="deab_register_post_settings[author]">Author</label>
                <input type="checkbox" name="deab_register_post_settings[author]" value="author" <?php echo (is_array($meta) && isset($meta['author'])) ? 'checked' : ''; ?>>
            </div>

            <!-- Revisions -->
            <div class="field-wrapper">
                <label for="deab_register_post_settings[revisions]">Revisions</label>
                <input type="checkbox" name="deab_register_post_settings[revisions]" value="revisions" <?php echo (is_array($meta) && isset($meta['revisions'])) ? 'checked' : ''; ?>>
            </div>

            <!-- Trackbacks -->
            <div class="field-wrapper">
                <label for="deab_register_post_settings[trackbacks]">Trackbacks</label>
                <input type="checkbox" name="deab_register_post_settings[trackbacks]" value="trackbacks" <?php echo (is_array($meta) && isset($meta['trackbacks'])) ? 'checked' : ''; ?>>
            </div>

            <!-- Custom Fields -->
            <div class="field-wrapper">
                <label for="deab_register_post_settings[custom-fields]">Custom Fields</label>
                <input type="checkbox" name="deab_register_post_settings[custom-fields]" value="custom-fields" <?php echo (is_array($meta) && isset($meta['custom-fields'])) ? 'checked' : ''; ?>>
            </div>

            <!-- Comments -->
            <div class="field-wrapper">
                <label for="deab_register_post_settings[comments]">Comments</label>
                <input type="checkbox" name="deab_register_post_settings[comments]" value="comments" <?php echo (is_array($meta) && isset($meta['comments'])) ? 'checked' : ''; ?>>
            </div>

            <!-- Page Attributes -->
            <div class="field-wrapper">
                <label for="deab_register_post_settings[page-attributes]">Page Attributes</label>
                <input type="checkbox" name="deab_register_post_settings[page-attributes]" value="page-attributes" <?php echo (is_array($meta) && isset($meta['page-attributes'])) ? 'checked' : ''; ?>>
            </div>

            <!-- Post Formats -->
            <div class="field-wrapper">
                <label for="deab_register_post_settings[post-formats]">Post Formats </label>
                <input type="checkbox" name="deab_register_post_settings[post-formats]" value="post-formats" <?php echo (is_array($meta) && isset($meta['post-formats'])) ? 'checked' : ''; ?>>
            </div>
        </section>

        <!-- Menu icon -->
        <section class="post-setting post-setting--menu-icon">
            <h2><strong>Menu Icon</strong></h2>

            <div class="field-wrapper">
                <input type="text" name="deab_register_post_settings[deab_dashicon]" id="deab_register_post_settings[deab_dashicon]" class="regular-text" placeholder="dashicon" value="<?php echo (is_array($meta) && isset($meta['deab_dashicon'])) ? $meta['deab_dashicon'] : ''; ?>">
                <p class="description">The url to the icon to be used for this menu. Add relevent icon text here "ex: dashicons-thumbs-up". <a href="https://developer.wordpress.org/resource/dashicons/">Dashicons</a> (Dashicons is the official icon font.)</p>
            </div>
        </section>

        <!-- Slug -->
        <section class="post-setting post-setting--rewrites">
            <h2><strong>Rewrites Settings</strong></h2>

            <div class="field-wrapper">
                <input type="text" name="deab_register_post_settings[deab_slug]" id="deab_register_post_settings[deab_slug]" class="regular-text" placeholder="slug" value="<?php echo (is_array($meta) && isset($meta['deab_slug'])) ? $meta['deab_slug'] : ''; ?>">
                <p class="description">Customize the permastruct slug. Defaults to Post type keyy.</p>
            </div>
        </section>

        <!-- Public -->
        <section class="post-setting post-setting--public">
            <h2><strong>Public</strong></h2>

            <select name="deab_register_post_settings[deab_public]" id="deab_register_post_settings[deab_public]">
                <option value="true" <?php if (is_array($meta) && isset($meta['deab_public']) && $meta['deab_public'] == 'true') {
                                            echo 'selected="selected"';
                                        } ?>>Yes</option>
                <option value="false" <?php if (is_array($meta) && isset($meta['deab_public']) && $meta['deab_public'] == 'false') {
                                            echo 'selected="selected"';
                                        } ?>>No</option>
            </select>
            <p class="description">Whether a post type is intended for use publicly either via the admin interface or by front-end users.</p>
        </section>

        <!-- Rest -->
        <section class="post-setting post-setting--rest">
            <h2><strong>Show in REST API</strong></h2>

            <select name="deab_register_post_settings[deab_rest]" id="deab_register_post_settings[deab_rest]">
                <option value="no" <?php if (is_array($meta) && isset($meta['deab_rest']) && $meta['deab_rest'] == 'no') {
                                        echo 'selected="selected"';
                                    } ?>>No</option>
                <option value="yes" <?php if (is_array($meta) && isset($meta['deab_rest']) && $meta['deab_rest'] == 'yes') {
                                        echo 'selected="selected"';
                                    } ?>>Yes</option>
            </select>
            <p class="description">Whether to include the post type in the REST API. Set this to true for the post type to be available in the block editor. </p>
        </section>

<?php }

    function save_deab_cpt_meta_fields($post_id)
    {
        // verify nonce
        if (isset($_POST['your_meta_box_nonce'])) :
            if (!wp_verify_nonce($_POST['your_meta_box_nonce'], basename(__FILE__))) {
                return $post_id;
            }
        endif;

        // check autosave
        if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) {
            return $post_id;
        }
        // check permissions
        if (isset($_POST['post_type'])) :
            if ('page' === $_POST['post_type']) {
                if (!current_user_can('edit_page', $post_id)) {
                    return $post_id;
                } elseif (!current_user_can('edit_post', $post_id)) {
                    return $post_id;
                }
            }
        endif;

        $old = get_post_meta($post_id, 'deab_register_post_settings', true);
        $new = '';

        if (isset($_POST['deab_register_post_settings'])) :
            $new = $_POST['deab_register_post_settings'];
        endif;

        if ($new && $new !== $old) {
            update_post_meta($post_id, 'deab_register_post_settings', $new);
        } elseif ('' === $new && $old) {
            delete_post_meta($post_id, 'deab_register_post_settings', $old);
        }
    }
}
