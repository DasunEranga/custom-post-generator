<?php

/**
 * @package deabPlugin 
 */

namespace Inc\Pages;

use function PHPSTORM_META\type;

class CustomPost
{

    public function register()
    {
        add_action('init', array($this, 'custom_post_type'));
    }


    public function deab_custom_post_settings()
    {
    }

    function custom_post_type()
    {
        $_post_type = esc_attr(get_option('deab_post_type'));
        $_post_label = esc_attr(get_option('deab_post_label'));

        register_post_type(
            'deab_cpt_generater',
            array(
                'labels' => array(
                    'name' => 'Custom Post Generator',
                    'singular_name' => $_post_label,
                    'add_new' => 'Add New CPT',
                    'add_new_item' => 'Add New Custom Post Type',
                ),
                'public' => true,
                'supports' => array('title'),
                'menu_position' => 110,
                'menu_icon'   => 'dashicons-buddicons-activity',
                'publicly_queryable' => true,
                'query_var' => true,
                'rewrite' => array('slug' => $_post_label),
                'capability_type' => 'post',
                'has_archive' => true,
                'exclude_from_search' => false,
                'delete_with_user' => false
            )
        );

        // taxonomy
        register_taxonomy(
            'taxonomy_creater',
            array('deab_cpt_generater'),
            array(
                'hierarchical' => true,
                'labels' => array(
                    'name' => 'Taxonomies',
                    'menu_name' => 'taxonomies',
                    'singular_name' => 'taxonomies',
                    'search_items' => 'Search taxonomies',
                    'all_items' => 'All taxonomies',
                    'parent_item' => 'Parent taxonomies',
                    'parent_item_colon' => 'Parent taxonomies:',
                    'edit_item' => 'Edit taxonomies',
                    'update_item' => 'Update taxonomies',
                    'add_new_item' => 'Add New taxonomies',
                    'new_item_name' => 'New taxonomies',
                ),
                'public' => false,
                'show_ui' => true,
                'show_admin_column' => true,
                'query_var' => true,
            )
        );

        // tags
        register_taxonomy(
            'tags_creater',
            array('deab_cpt_generater'),
            array(
                'hierarchical' => true,
                'labels' => array(
                    'name' => 'Tags',
                    'menu_name' => 'Tags',
                    'singular_name' => 'Tags',
                    'search_items' => 'Search Tags',
                    'all_items' => 'All Tags',
                    'parent_item' => 'Parent Tags',
                    'parent_item_colon' => 'Parent Tags:',
                    'edit_item' => 'Edit Tags',
                    'update_item' => 'Update Tags',
                    'add_new_item' => 'Add New Tags',
                    'new_item_name' => 'New Tags',
                ),
                'public' => false,
                'show_ui' => true,
                'show_admin_column' => true,
                'query_var' => true,
            )
        );

        global $post;
        $args = array(
            'post_type' => 'deab_cpt_generater',
            'posts_per_page' => -1
        );
        $myposts = get_posts($args);

        foreach ($myposts as $post) :

            setup_postdata($post);

            $meta = get_post_meta($post->ID, 'deab_register_post_settings', true);

            // post basic
            if (!empty($meta['deab_post'])) {
                $_post_key = $meta['deab_post'];
            }

            // menu position
            if (!empty($meta['deab_menu_position'])) {
                $_menu_position = $meta['deab_menu_position'];
                $_menu_position = intval($_menu_position);
            } else {
                $_menu_position = 25;
            }

            // post support
            if (!empty($meta['deab_dashicon'])) {
                $_dashicon = $meta['deab_dashicon'];
            } else {
                $_dashicon = 'dashicons-admin-post';
            }
            if (!empty($meta['editor'])) {
                $_editor = $meta['editor'];
            } else {
                $_editor = '';
            }
            if (!empty($meta['author'])) {
                $_author = $meta['author'];
            } else {
                $_author = '';
            }
            if (!empty($meta['thumbnail'])) {
                $_thumbnail = $meta['thumbnail'];
            } else {
                $_thumbnail = '';
            }
            if (!empty($meta['excerpt'])) {
                $_excerpt = $meta['excerpt'];
            } else {
                $_excerpt = '';
            }
            if (!empty($meta['trackbacks'])) {
                $_trackbacks = $meta['trackbacks'];
            } else {
                $_trackbacks = '';
            }
            if (!empty($meta['custom-fields'])) {
                $_custom_fields = $meta['custom-fields'];
            } else {
                $_custom_fields = '';
            }
            if (!empty($meta['comments'])) {
                $_comments = $meta['comments'];
            } else {
                $_comments = '';
            }
            if (!empty($meta['revisions'])) {
                $_revisions = $meta['revisions'];
            } else {
                $_revisions = '';
            }
            if (!empty($meta['page-attributes'])) {
                $_page_attributes = $meta['page-attributes'];
            } else {
                $_page_attributes = '';
            }
            if (!empty($meta['post-formats'])) {
                $_post_formats = $meta['post-formats'];
            } else {
                $_post_formats = '';
            }

            // Slug
            if (!empty($meta['deab_slug'])) {
                $_post_slug = $meta['deab_slug'];
            } else {
                $_post_slug = $_post_key;
            }

            // Public
            if (!empty($meta['deab_public'])) {
                $_public = $meta['deab_public'];
                if ($_public == "true") {
                    $_public = true;
                }
            }

            // Rest
            $_rest = false;
            if (!empty($meta['deab_rest'])) {
                $_rest = $meta['deab_rest'];
                if ($_rest == "true") {
                    $_rest = true;
                }
            }

            register_post_type(
                $_post_key,
                array(
                    'public' => $_public,
                    'labels' => array(
                        'name' => $post->post_title,
                        'singular_name' => $post->post_title,
                        'add_new' => 'Add New',
                        'add_new_item' => 'Add New',
                    ),
                    'supports' => array('title', $_editor, $_author, $_thumbnail, $_excerpt, $_trackbacks, $_custom_fields, $_comments, $_revisions, $_page_attributes, $_post_formats),
                    'menu_position' => $_menu_position,
                    'menu_icon'   => $_dashicon,
                    'rewrite' => array('slug' => $_post_slug),
                    'show_in_rest' => $_rest
                )
            );

            // hierarchical taxonomy (custom taxonomy)
            $_taxonomy = wp_get_post_terms($post->ID, 'taxonomy_creater');

            foreach ($_taxonomy as $term) {

                register_taxonomy(
                    $term->slug,
                    array($_post_key),
                    array(
                        'hierarchical' => true,
                        'labels' => array(
                            'name' => $term->name,
                            'menu_name' => $term->name,
                            'singular_name' => $term->name,
                            'search_items' => 'Search ' . $term->name,
                            'all_items' => 'All ' . $term->name,
                            'parent_item' => 'Parent ' . $term->name,
                            'parent_item_colon' => 'Parent ' . $term->name,
                            'edit_item' => 'Edit ' . $term->name,
                            'update_item' => 'Update ' . $term->name,
                            'add_new_item' => 'Add New ' . $term->name,
                            'new_item_name' => 'New ' . $term->name,
                        ),
                        'public' => true,
                        'show_ui' => true,
                        'show_admin_column' => true,
                        'query_var' => true,
                    )
                );
            }

            // tags
            $_tags = wp_get_post_terms($post->ID, 'tags_creater');
            foreach ($_tags as $term) {

                register_taxonomy(
                    $term->slug,
                    array($_post_key),
                    array(
                        'label' => $term->name,
                        'hierarchical' => false,
                    )
                );
            }

            flush_rewrite_rules();

        endforeach;

        wp_reset_postdata();
    }
}
