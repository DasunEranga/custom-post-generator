<h1>Deab Plugin Admin Page</h1>

<?php settings_errors(); ?>

<form action="options.php" method="post">
    <?php settings_fields('deab-setting-group'); ?>
    <?php do_settings_sections('deab_plugin'); ?>
    <?php submit_button(); ?>

</form>