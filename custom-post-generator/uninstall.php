<?php

/**
 * Trigger this file on deab plugin unistalation...
 * 
 * @package deabPlugin 
 * 
 */

if (!defined('WP_UNINSTALL_PLUGIN')) {
    die;
}

// Clear database storage

// Method 2: Access the data base via SQL
global $wpdb;

// $wpdb->query("DELETE FROM wp_posts WHERE post_type='location'");
// $wpdb->query("DELETE FROM wp_postmeta WHERE post_id NOT IN (SELECT ID FROM wp_posts)");
// $wpdb->query("DELETE FROM wp_term_relationships WHERE object_id NOT IN (SELECT ID FROM wp_posts)");
